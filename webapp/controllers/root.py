# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound

from webapp.lib.base import BaseController
from webapp.controllers.error import ErrorController
from webapp.lib.app_globals import Globals

__all__ = ['RootController']


class RootController(BaseController):
    """
    The root controller for the webapp application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """

    error = ErrorController()
    

    def _before(self, *args, **kw):
        tmpl_context.project_name = "webapp"

    @expose('index.jinja')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('info.jinja')
    def info(self):
        return dict(page='info')
    
    @expose('existingorg.jinja')
    def existingorg(self):
        orgs = Globals.sp.getOrganisationNames()
        return dict(orgs = orgs)
    
    @expose('neworg.jinja')
    def neworg(self):
        return dict(page='neworg')

    @expose('webapp.templates.data')
    @expose('json')
    def data(self, **kw):
        """
        This method showcases how you can use the same controller
        for a data page and a display page.
        """
        return dict(page='data', params=kw)
    
    @expose('login.jinja')
    def login(self):
        """Handle the front-page."""
        return dict(page='login')

